// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract SETToken {
    string public tokenName;
    string public tokenSymbol;
    uint8 public tokenDecimals;
    uint256 public tokenTotalSupply;

    mapping(address => uint256) public tokenBalanceOf;
    mapping(address => mapping(address => uint256)) public allowance;

    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);

    constructor(string memory _name, string memory _symbol) {
        tokenName = _name;
        tokenSymbol = _symbol;
        tokenDecimals = 18;
        tokenTotalSupply = 1000000 * 10**uint256(tokenDecimals);
        tokenBalanceOf[msg.sender] = tokenTotalSupply;
    }

    function name() external view returns (string memory) {
        return tokenName;
    }

    function balanceOf(address account) external view returns (uint256) {
        return tokenBalanceOf[account];
    }

    function totalSupply() external view returns (uint256) {
        return tokenTotalSupply;
    }

    function decimals() external view returns (uint8) {
        return tokenDecimals;
    }

    function symbol() external view returns (string memory) {
        return tokenSymbol;
    }

    function transfer(address to, uint256 value) external returns (bool) {
        require(tokenBalanceOf[msg.sender] >= value, "No balance for this transfer");
        tokenBalanceOf[msg.sender] -= value;
        tokenBalanceOf[to] += value;
        emit Transfer(msg.sender, to, value);
        return true;
    }

    function transferFrom(address from, address to, uint256 value) external returns (bool) {
        require(tokenBalanceOf[from] >= value, "No balance for this transfer");
        require(allowance[from][msg.sender] >= value, "No allow for this balance");
        tokenBalanceOf[from] -= value;
        tokenBalanceOf[to] += value;
        allowance[from][msg.sender] -= value;
        emit Transfer(from, to, value);
        return true;
    }

    function approve(address spender, uint256 value) external returns (bool) {
        allowance[msg.sender][spender] = value;
        emit Approval(msg.sender, spender, value);
        return true;
    }
}


contract GenarateTokenContract {
    SETToken public myToken;

    constructor() {
        myToken = new SETToken("SETRST", generateSymbol("SETRST"));
    }

    function generateSymbol(string memory _name) internal pure returns (string memory) {
        return string(abi.encodePacked(_name, "RST"));
    }
}
